namespace Neo4jTraining.Generator
{
    using System;
    using System.Collections.Generic;

    using Neo4jClient;

    using Neo4jTraining.Tests;

    public class CypherCreateQuery
    {
        private GraphClient graphClient;

        public CypherCreateQuery()
        {
            this.graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"));
            this.graphClient.Connect();
        }

        public void CreateNodes(List<Product> products)
        {
            products.ForEach(
                i =>
                this.graphClient.Cypher.Create(
                    string.Format(
                        "(product:Product {{Id : '{0}', CategoryName : '{1}'}})",
                        i.Id, i.CategoryName)).ExecuteWithoutResults());
        }

        public void CreateRelations(List<ProductBought> productBought)
        {
            productBought.ForEach(
                i =>
                this.graphClient.Cypher.Match("(productA:Product),(productB:Product)").Where(string.Format("productA.Id = '{0}' and productB.Id = '{1}'", i.ProductA.Id, i.ProductB.Id)).Create(
                    string.Format(
                        "(productA)-[r:BOUGHT_WITH {{Times:{0}}}]->(productB)",
                        i.Times)).ExecuteWithoutResults());
        }
    }
}