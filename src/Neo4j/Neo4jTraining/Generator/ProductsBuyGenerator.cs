﻿namespace Neo4jTraining.Generator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Neo4jTraining.Tests;

    public class ProductsBuyGenerator
    {
        public void GenerateBoughtWithGroup(int productInGroup)
        {
            var myArray = new Product[productInGroup];
            var rand = new Random(5);

            var categoryRandom = new Random(1);

            string[] categories = { "AGD", "Chemical", "Plumbing" };

            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = new Product {
                    Id = Guid.NewGuid(),
                    CategoryName = categories[categoryRandom.Next(0, 2)]
                };
            }

            var productBought = new List<ProductBought>();

            for (int x = 0; x < myArray.Length; ++x)
            {
                for (int y = x + 1; y < myArray.Length; ++y)
                {
                    var newProductBought = new ProductBought(myArray[x], myArray[y]);

                    if (x > myArray.Length / 2)
                    {
                        newProductBought.Times = rand.Next(0, 50);
                    }

                    productBought.Add(newProductBought);
                }
            }

            var createQuery = new CypherCreateQuery();

            createQuery.CreateNodes(myArray.ToList());
            createQuery.CreateRelations(productBought);
        }
    }
}