﻿namespace Neo4jTraining.Generator
{
    using Neo4jTraining.Tests;

    public class ProductBought
    {
        public override bool Equals(object obj)
        {
            var other = obj as ProductBought;

            if (other == null)
            {
                return false;
            }

            return other.ProductA == this.ProductB && other.ProductB == this.ProductA || other.ProductA == this.ProductA && other.ProductB == this.ProductB;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public Product ProductA { get; private set; }

        public Product ProductB { get; private set; }

        public int Times { get; set; }

        public ProductBought(Product productA, Product productB)
        {
            this.ProductB = productB;
            this.ProductA = productA;
        }
    }
}