﻿namespace Neo4jTraining.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Neo4jClient;
    using Neo4jClient.Cypher;

    using NUnit.Framework;

    [TestFixture]
    public class Tests
    {
        private GraphClient client;

        [SetUp]
        public void Setup()
        {
            this.client = new GraphClient(new Uri("http://localhost:7474/db/data"));

            this.client.Connect();
        }

        [Test]
        public void ShouldAddAndReturnNode()
        {
            var guid = Guid.NewGuid();
            this.CreateNewNode(guid);

            var node = this.GetNodeWithCyphrt(guid).Return<Product>("(product)");

            Assert.AreEqual(guid, node.Results.First().Id);
        }

        private ICypherFluentQuery GetNodeWithCyphrt(Guid guid)
        {
            return this.client.Cypher.Match("(product:Product)").Where("product.Id ='" + guid + "'");
        }

        private void CreateNewNode(Guid guid)
        {
            this.client.Cypher.Create("(product:Product {newProduct})")
                .WithParam("newProduct", new Product() { Id = guid, CategoryName = "Phones" }).ExecuteWithoutResults();
        }

        [Test]
        public void ShouldAddRelationBetweenProducts()
        {
            var sourceId = Guid.NewGuid();
            var targetId = Guid.NewGuid();

            this.CreateNewNode(sourceId);
            this.CreateNewNode(targetId);

            var sourceNode = this.GetNodeWithCyphrt(sourceId).Return<Node<Product>>("(product)").Results.First();

            var targetNode = this.GetNodeWithCyphrt(targetId).Return<Node<Product>>("(product)").Results.First();

            this.client.CreateRelationship(
                sourceNode.Reference,
                new BoughtWith(targetNode.Reference, new Payload(){ Times = 1 }));

            var boughtWiths = this.client.Cypher.Match("(:Product) -[b:BOUGHT_WITH]->(:Product)")
                .Return(b => new { D = b.As<RelationshipInstance<Payload>>()})
                .Results;

            Assert.Inconclusive();
        }
    }

    public class Payload
    {
        public int Times { get; set; }
    }
    public class BoughtWith : Relationship, IRelationshipAllowingSourceNode<Product>, IRelationshipAllowingTargetNode<Product>
    {
        
        public BoughtWith(NodeReference targetNode)
            : base(targetNode)
        {
        }
        
        public BoughtWith(NodeReference targetNode, object data)
            : base(targetNode, data)
        {
        }

        public override string RelationshipTypeKey
        {
            get
            {
                return "BOUGHT_WITH";
            }
        }
    }

    public class Product
    {
        public string CategoryName { get; set; }

        public Guid Id { get; set; }
    }
}