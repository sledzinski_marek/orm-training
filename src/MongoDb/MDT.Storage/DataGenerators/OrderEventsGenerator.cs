﻿namespace MDT.Storage.DataGenerators
{
    using System;
    using System.Globalization;
    using System.Linq;

    using MDT.Models.Events;
    using MDT.Storage.Cotnext;

    internal static class OrderEventsGenerator
    {
        private const int NumberOfAdds = 10;

        private const int NumberOfRemoves = 5;

        private const int DivRandom = 4;

        public static void GenerateAndInsertToCollection(
            MongoClientContext context,
            string collectionName)
        {
            var collection = context.FetchCollection(collectionName);

            collection.RemoveAll();

            for (var counter = 0; counter < 25; counter++)
            {
                var orderId = Guid.NewGuid();
                var customerId = Guid.NewGuid();

                var products =
                    Enumerable.Range(1, 10)
                        .Select(
                            i =>
                            new OrderItem(
                                Guid.NewGuid(),
                                new Money(10, Currency.PL),
                                "Category_" + (i % DivRandom).ToString(CultureInfo.InvariantCulture),
                                i % 3)).ToList();

                var added = Enumerable.Range(0, NumberOfAdds).Select(
                    i => new CustomerAddedItemToCart(
                             orderId,
                             customerId,
                             products[i],
                             DateTime.UtcNow));

                //var removed = Enumerable.Range(DivRandom, NumberOfRemoves).Select(
                //            i => new CustomerRemovedItemFromCart(
                //                            orderId,
                //                            customerId,
                //                            products[i % DivRandom],
                //                            i,
                //                            DateTime.UtcNow,
                //                            i));

                collection.InsertBatch(added);
                //collection.InsertBatch(removed);
            }
        }
    }

}