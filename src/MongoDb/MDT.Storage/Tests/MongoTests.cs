﻿namespace MDT.Storage.Tests
{
    using System;
    using System.Linq;
    using System.Runtime.Remoting.Contexts;

    using MDT.Models.Events;
    using MDT.Storage.Cotnext;

    using MongoDB.Driver;
    using MongoDB.Driver.Builders;

    using NUnit.Framework;

    [TestFixture]
    public class MongoTests
    {
        private const string NameOfDatabase = "Carts";

        private const string NameOfCollection = "CartChanges";

        [Test]
        public void ShouldAddDocumentToMongoDb()
        {
            // TODO: look thru the code
            // given
            var removedEvent = new CustomerAddedItemToCart(
                Guid.NewGuid(),
                Guid.NewGuid(),
                new OrderItem(Guid.NewGuid(), new Money(1, Currency.PL), "cat", 1),
                DateTime.UtcNow);

            var context = MongoClientContext.CreateNewClientContext(NameOfDatabase);
            var collection = context.FetchCollection(NameOfCollection);

            // when 
            // remove all items from collection
            collection.RemoveAll();

            // insert event item
            collection.Insert(removedEvent);

            // find first result of given type
            var result = collection.FindOneAs<CustomerAddedItemToCart>();

            // then
            Assert.That(result.CustomerId, Is.EqualTo(removedEvent.CustomerId));
        }

        [Test]
        public void ShouldFindCustomersRankInSavedEvent()
        {
            // given 
            var orderId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            var customerRank = 10;

            var removeableItem = new OrderItem(Guid.NewGuid(), new Money(100, Currency.PL), "cat", 2);

            var context = MongoClientContext.CreateNewClientContext(NameOfDatabase);
            var collection = context.FetchCollection(NameOfCollection);

            // when 
            collection.RemoveAll();

            // TODO: add one removed event to log

            // TODO: fetch removed item
            // var fetchedItem = null;

            // Assert.That(customerRank, Is.EqualTo(fetchedItem.Rank));
        }

        [Test]
        public void ShouldSumUpWholeCart()
        {
            // given 
            var orderId = Guid.NewGuid();
            var customerId = Guid.NewGuid();

            var removeableItem = new OrderItem(Guid.NewGuid(), new Money(100, Currency.PL), "cat", 2);
            var addedItem = new OrderItem(Guid.NewGuid(), new Money(200, Currency.PL), "cat", 3);

            var context = MongoClientContext.CreateNewClientContext(NameOfDatabase);
            var collection = context.FetchCollection(NameOfCollection);

            // when 
            collection.RemoveAll();

            collection.Insert(
                new CustomerAddedItemToCart(orderId, customerId, addedItem, DateTime.UtcNow));

            collection.Insert(
                new CustomerAddedItemToCart(orderId, customerId, removeableItem, DateTime.UtcNow));

            // TODO: add one 'removed event' to log

            // TODO: fetch the data and calcualte total
            // hint: AsQueryable<CustomerCartChangedBase>() might help
            var result = 0;

            // then
            Assert.That(result, Is.EqualTo(600));
        }


        [Test]
        [Ignore("Map Reduce with MongoDB - Advanced stuff :)")]
        public void ShouldFindHowManyTimesCategoryWasRemoved()
        {
            // given
            var context = MongoClientContext.CreateNewClientContext(NameOfDatabase);
            DataGenerators.OrderEventsGenerator.GenerateAndInsertToCollection(context, NameOfCollection);

            var collection = context.FetchCollection(NameOfCollection);

            // TODO: write proper map-reduce functions
            var mapFunction = @"function() {
                                        emit(some_key, some_value);
                                   }";

            var reduceFunction = @"function(key, values) {
                                        return values;
                                   }";

            var args = new MapReduceArgs
            {
                MapFunction = mapFunction,
                ReduceFunction = reduceFunction,
                OutputMode = MapReduceOutputMode.Replace,
                OutputCollectionName = "Reduced",
                OutputDatabaseName = NameOfDatabase,
                Query = Query.Where("this['_t'][1] === 'Removed'") // not the best way but will do for now :)
            };

            // hint: output document format
            // hint: easier to create model class and create a mapping to have type map-reduce results
            // { "_id" : "Category", "value" : { "count" : 225 } }

            // when
            var result = collection.MapReduce(args);

            // TODO: get the resutls back and look for category name with max count value
            var resultCategory = string.Empty;            

            // then
            Assert.That(resultCategory, Is.EqualTo("Category_1"));
        }
    }
}