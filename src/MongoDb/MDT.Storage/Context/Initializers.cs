﻿namespace MDT.Storage.Cotnext
{
    using MDT.Models.Events;

    using MongoDB.Bson;

    public static class Initializers
    {
        public static void EnsureKnownTypesAreMapped()
        {
            if (MongoDB.Bson.Serialization.BsonClassMap.IsClassMapRegistered(typeof(CustomerCartChangedBase)))
            {
                return;
            }

            MongoDB.Bson.Serialization.BsonClassMap.RegisterClassMap<Money>(
                cm =>
                    {
                        cm.MapProperty(r => r.Value);
                        cm.MapProperty(r => r.Currency);

                        cm.MapCreator(r => new Money(r.Value, r.Currency));
                    });

            MongoDB.Bson.Serialization.BsonClassMap.RegisterClassMap<OrderItem>(
                cm =>
                    {
                        cm.MapProperty(r => r.ProductId).SetRepresentation(BsonType.String);
                        cm.MapProperty(r => r.ProductCategory);
                        cm.MapProperty(r => r.Quantity);
                        cm.MapProperty(r => r.Price);

                        cm.MapCreator(r => new OrderItem(r.ProductId, r.Price, r.ProductCategory, r.Quantity));
                    });

            MongoDB.Bson.Serialization.BsonClassMap.RegisterClassMap<CustomerCartChangedBase>(
                cm =>
                    {
                        cm.SetIsRootClass(true);
                        cm.SetDiscriminator("Base");
                        cm.MapProperty(r => r.PublishedOnUtc);
                        cm.MapProperty(r => r.OrderId).SetRepresentation(BsonType.String);
                        cm.MapProperty(r => r.Item);
                        cm.MapProperty(r => r.CustomerId).SetRepresentation(BsonType.String);

                        cm.MapCreator(
                            r =>
                            new CustomerCartChangedBase(
                                r.OrderId,
                                r.CustomerId,
                                r.Item,
                                r.PublishedOnUtc));

                        cm.AddKnownType(typeof(CustomerAddedItemToCart));
                    });

            MongoDB.Bson.Serialization.BsonClassMap.RegisterClassMap<CustomerAddedItemToCart>(
                cm =>
                    {
                        cm.SetDiscriminator("Added");
                        cm.MapCreator(
                            r =>
                            new CustomerAddedItemToCart(
                                r.OrderId,
                                r.CustomerId,
                                r.Item,
                                r.PublishedOnUtc));
                    });

        }
    }

}