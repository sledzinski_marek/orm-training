﻿namespace MDT.Storage.Cotnext
{
    using System;

    using MongoDB.Driver;
    using MongoDB.Driver.Builders;

    public class MongoClientContext
    {
        private readonly MongoClient contextClient;

        private MongoDatabase connectedDatabase;

        private MongoClientContext(MongoDatabase database, MongoClient contextClient)
        {
            this.connectedDatabase = database;
            this.contextClient = contextClient;
        }

        public static MongoClientContext CreateNewClientContext(string databaseName, string connectionString = null)
        {
            var mongoClient = string.IsNullOrEmpty(connectionString)
                                  ? new MongoClient()
                                  : new MongoClient(connectionString);
            var database = GetDatabaseWithName(mongoClient, databaseName);

            Initializers.EnsureKnownTypesAreMapped();

            return new MongoClientContext(database, mongoClient);
        }

        public MongoCollection FetchCollection(string collectionName)
        {
            this.EnsureCollectionExists(collectionName);

            return this.connectedDatabase.GetCollection(collectionName, WriteConcern.Unacknowledged);
        }

        public void DropCurrentDatabase()
        {
            var databaseName = this.connectedDatabase.Name;
            this.connectedDatabase.Drop();
            this.connectedDatabase = GetDatabaseWithName(this.contextClient, databaseName);
        }

        private static MongoDatabase GetDatabaseWithName(MongoClient context, string databaseName)
        {
            return context.GetServer().GetDatabase(databaseName);
        }

        private void EnsureCollectionExists(string collectionName)
        {
            if (this.connectedDatabase.CollectionExists(collectionName))
            {
                return;
            }

            var collectionOption = CollectionOptions.SetAutoIndexId(true);

            if (!this.connectedDatabase.CreateCollection(collectionName, collectionOption).Ok)
            {
                throw new ApplicationException("Failed to create collection");
            }
        }
    }
}
