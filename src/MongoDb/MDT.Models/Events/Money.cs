﻿namespace MDT.Models.Events
{
    using System;

    public class Money
    {
        private readonly decimal value;

        private readonly Currency currency;

        public Money(decimal value, Currency currency)
        {
            this.value = value;
            this.currency = currency;
        }

        public decimal Value
        {
            get
            {
                return this.value;
            }
        }

        public Currency Currency
        {
            get
            {
                return this.currency;
            }
        }

        public static Money Empty(Currency currency)
        {
            return new Money(0, currency);
        }

        public static Money Add(Money first, Money second)
        {
            if (second.Currency != first.Currency)
            {
                throw new ArgumentException("Currency is different.");
            }

            return new Money(second.Value + first.Value, first.Currency);
        }
        
        protected bool Equals(Money other)
        {
            return this.value == other.value && this.currency == other.currency;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((Money)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.value.GetHashCode() * 397) ^ (int)this.currency;
            }
        }

        public static bool operator ==(Money left, Money right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Money left, Money right)
        {
            return !Equals(left, right);
        }

        public static Money operator +(Money left, Money right)
        {
            return Money.Add(left, right);
        }

        public override string ToString()
        {
            return string.Format("Value: {0}, Currency: {1}", this.Value, this.Currency);
        }
    }
}