﻿namespace MDT.Models.Events
{
    using System;

    public interface IVersionedDomainEvent
    {
        DateTime PublishedOnUtc { get; }
    }
}