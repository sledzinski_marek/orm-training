﻿namespace MDT.Models.Events
{
    using System;

    public class CustomerCartChangedBase : IVersionedDomainEvent
    {
        private readonly Guid orderId;

        private readonly Guid customerId;

        private readonly OrderItem item;

        private readonly DateTime publishedOnUtc;

        public CustomerCartChangedBase(
            Guid orderId,
            Guid customerId,
            OrderItem item,
            DateTime publishedOnUtc)
        {
            this.orderId = orderId;
            this.customerId = customerId;
            this.item = item;
            this.publishedOnUtc = publishedOnUtc;
        }

        public int Id { get; set; }

        public Guid OrderId
        {
            get
            {
                return this.orderId;
            }
        }

        public Guid CustomerId
        {
            get
            {
                return this.customerId;
            }
        }

        public OrderItem Item
        {
            get
            {
                return this.item;
            }
        }

        public DateTime PublishedOnUtc
        {
            get
            {
                return this.publishedOnUtc;
            }
        }

    }
}