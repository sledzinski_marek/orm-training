﻿namespace MDT.Models.Events
{
    using System;

    public class OrderItem
    {
        private readonly Guid productId;

        private readonly string productCategory;

        private readonly Money price;

        private readonly int quantity;

        public OrderItem(Guid productId, Money price, string productCategory, int quantity)
        {
            this.productId = productId;
            this.price = price;
            this.productCategory = productCategory;
            this.quantity = quantity;
        }

        public Guid ProductId
        {
            get
            {
                return this.productId;
            }
        }

        public Money Price
        {
            get
            {
                return this.price;
            }
        }

        public string ProductCategory
        {
            get
            {
                return this.productCategory;
            }
        }

        public int Quantity
        {
            get
            {
                return this.quantity;
            }
        }
        
        public static bool operator ==(OrderItem left, OrderItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(OrderItem left, OrderItem right)
        {
            return !Equals(left, right);
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((OrderItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = this.productId.GetHashCode();
                hashCode = (hashCode * 397) ^ (this.productCategory != null ? this.productCategory.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.price != null ? this.price.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.quantity;
                return hashCode;
            }
        }

        protected bool Equals(OrderItem other)
        {
            return
                this.productId.Equals(other.productId) &&
                string.Equals(this.productCategory, other.productCategory) &&
                Equals(this.price, other.price) &&
                this.quantity == other.quantity;
        }
    }
}