﻿namespace MDT.Models.Events
{
    using System;

    public class CustomerAddedItemToCart : CustomerCartChangedBase
    {
        public CustomerAddedItemToCart(Guid orderId, Guid customerId, OrderItem item, DateTime publishedOnUtc)
            : base(orderId, customerId, item, publishedOnUtc)
        {
        }
    }
}