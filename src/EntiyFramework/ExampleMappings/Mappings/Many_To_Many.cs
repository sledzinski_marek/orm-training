﻿namespace Mappings
{
    using System.Data.Entity.ModelConfiguration;

    public class Many_To_Many : EntityTypeConfiguration<Instructor>
    {
        public Many_To_Many()
        {
            this.HasMany(i => i.OfficeAssignments).WithMany(o => o.Instructors).Map(
                m =>
                    {
                        m.MapLeftKey("Id");
                        m.MapRightKey("Id");
                        m.ToTable("InstructorsAndAssigments");
                    });
        }
    }
}
