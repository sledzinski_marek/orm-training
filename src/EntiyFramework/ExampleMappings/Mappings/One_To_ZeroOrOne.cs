﻿namespace Mappings
{
    using System.Data.Entity.ModelConfiguration;

    public class One_To_ZeroOrOne : EntityTypeConfiguration<OfficeAssignment>
    {
        public One_To_ZeroOrOne()
        {
            this.HasRequired(o => o.Instructor).WithOptional(i => i.OfficeAssignment);
        }
    }
}