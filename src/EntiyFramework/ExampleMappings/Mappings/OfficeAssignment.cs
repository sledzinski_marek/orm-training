﻿namespace Mappings
{
    using System.Collections.Generic;

    public class OfficeAssignment
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual Instructor Instructor { get; set; }

        public virtual ICollection<Instructor>  Instructors { get; set; }
    }
}