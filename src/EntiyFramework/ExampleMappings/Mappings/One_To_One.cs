﻿namespace Mappings
{
    using System.Data.Entity.ModelConfiguration;

    public class One_To_One : EntityTypeConfiguration<Instructor>
    {
        public One_To_One()
        {
            this.HasRequired(i => i.OfficeAssignment).WithRequiredPrincipal(o => o.Instructor);
        }
    }
}
