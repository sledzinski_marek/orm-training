﻿namespace Mappings
{
    using System.Collections.Generic;

    public class Instructor
    {
        public int Id { get; set; }

        public virtual OfficeAssignment OfficeAssignment { get; set; }

        public virtual ICollection<OfficeAssignment> OfficeAssignments { get; set; }
    }
}