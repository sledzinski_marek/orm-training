﻿namespace Mappings
{
    using System.Data.Entity.ModelConfiguration;

    public class One_To_Many : EntityTypeConfiguration<Instructor>
    {
        public One_To_Many()
        {
            this.HasMany(i => i.OfficeAssignments).WithRequired(o => o.Instructor);

            // or if FK nullable
            this.HasMany(i => i.OfficeAssignments).WithOptional(o => o.Instructor);
        }
    }
}
