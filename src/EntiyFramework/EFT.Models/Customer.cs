﻿namespace EFT.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class Customer
    {
        public Customer(
            string name,
            Address invoicingAddress, 
            ICollection<CustomerGroup> groups = null)
        {
            this.Name = name;
            this.InvoicingAddress = invoicingAddress;
            this.Groups = groups ?? new Collection<CustomerGroup>();
            this.CreatedOn = DateTime.UtcNow;
        }

        public Customer(
            int referenceId,
            string name,
            Address invoicingAddress,
            ICollection<CustomerGroup> groups = null)
            : this(name, invoicingAddress, groups)
        {
            this.Id = referenceId;
        }

        private Customer()
        {
            // empty constructor required by EF :(
        }

        public int Id { get; private set; }
        
        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }

        public Address InvoicingAddress { get; set; }

        public virtual ICollection<CustomerGroup> Groups { get; private set; }
    }
}