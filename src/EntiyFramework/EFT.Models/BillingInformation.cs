﻿namespace EFT.Models
{
    using System;

    public abstract class BillingInformation
    {
        protected BillingInformation(Customer owner) 
        {
            this.Owner = owner;
        }

        protected BillingInformation(int ownerId)
        {
            this.OwnerId = ownerId;
        }

        protected BillingInformation()
        {
            // empty constructor required by EF :(
        }

        public int Id { get; private set; }

        public virtual Customer Owner { get; set; }

        public int OwnerId { get; set; }
    }
}