﻿namespace EFT.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class CustomerGroup
    {
        public CustomerGroup(string name, ICollection<Customer> customersInGroup = null)
        {
            this.Name = name;
            this.CustomersInGroup = customersInGroup ?? new Collection<Customer>();
            this.ActiveUntil = DateTime.UtcNow.AddMonths(6);
        }

        private CustomerGroup()
        {
            // empty constructor required by EF :(
        }

        public int Id { get; private set; }

        public string Name { get; set; }

        public virtual ICollection<Customer> CustomersInGroup { get; private set; }

        public DateTime ActiveUntil { get; set; }
    }
}