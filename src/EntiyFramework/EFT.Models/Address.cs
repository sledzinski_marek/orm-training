﻿namespace EFT.Models
{
    public class Address
    {
        private static readonly Address EmptyAddressValue = new Address(string.Empty, string.Empty, string.Empty);

        public Address(string country, string city, string postalCode)
        {
            this.Country = country;
            this.City = city;
            this.PostalCode = postalCode;
        }

        private Address()
        {
            // empty constructor required by EF :(
        }

        public string Country { get; private set; }

        public string City { get; private set; }

        public string PostalCode { get; private set; }

        public bool IsEmpty
        {
            get
            {
                return Equals(this, EmptyAddressValue);
            }
        }

        public Address WithData(string country, string city = null, string postalCode = null)
        {
            return new Address(country, city ?? this.City, postalCode ?? this.PostalCode);    
        }

        public static Address Empty()
        {
            return EmptyAddressValue;
        }

        public static bool operator ==(Address left, Address right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Address left, Address right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == this.GetType() && this.Equals((Address)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (this.Country != null ? this.Country.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.City != null ? this.City.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.PostalCode != null ? this.PostalCode.GetHashCode() : 0);
                return hashCode;
            }
        }

        protected bool Equals(Address other)
        {
            return string.Equals(this.Country, other.Country) &&
                    string.Equals(this.City, other.City) && 
                    string.Equals(this.PostalCode, other.PostalCode);
        }
    }
}