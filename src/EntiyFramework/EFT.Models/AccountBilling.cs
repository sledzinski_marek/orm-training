﻿namespace EFT.Models
{
    using System;

    public class AccountBilling : BillingInformation
    {
        public AccountBilling(Customer owner, string accountNumber, string bankSwiftNumber)
            : base(owner)
        {
            this.AccountNumber = accountNumber;
            this.BankSwiftNumber = bankSwiftNumber;
        }

        public AccountBilling(int ownerId, string accountNumber, string bankSwiftNumber)
            : base(ownerId)
        {
            this.AccountNumber = accountNumber;
            this.BankSwiftNumber = bankSwiftNumber;
        }

        private AccountBilling() : base()
        {
            // empty constructor required by EF :(
        }

        public string AccountNumber { get; set; }

        public string BankSwiftNumber { get; set; }
    }
}