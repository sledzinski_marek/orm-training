﻿namespace EFT.Models
{
    using System;

    public class CreditCardBilling : BillingInformation
    {
        private readonly TimeSpan minimalExpirationSpan = TimeSpan.FromHours(1);

        public CreditCardBilling(Customer owner, string cardNumber, int expiryMonth, int expiryYear)
            : base(owner)
        {
            this.CardNumber = cardNumber;
            this.ExpiryMonth = expiryMonth;
            this.ExpiryYear = expiryYear;
        }

        public CreditCardBilling(int ownerId, string cardNumber, int expiryMonth, int expiryYear)
            : base(ownerId)
        {
            this.CardNumber = cardNumber;
            this.ExpiryMonth = expiryMonth;
            this.ExpiryYear = expiryYear;
        }

        private CreditCardBilling() : base()
        {
            // empty constructor required by EF :(
        }

        public string CardNumber { get; set; }
        
        public int ExpiryMonth { get; set; }

        public int ExpiryYear { get; set; }

        public bool IsExpired
        {
            get
            {
                return
                    new DateTime(
                        ExpiryYear, 
                        ExpiryMonth, 
                        DateTime.DaysInMonth(ExpiryYear, ExpiryMonth)).Subtract(DateTime.UtcNow) < minimalExpirationSpan;
            }
        }
    }
}