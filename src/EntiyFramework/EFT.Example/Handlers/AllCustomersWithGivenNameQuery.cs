﻿namespace EFT.Example.Handlers
{
    public class AllCustomersWithGivenNameQuery : IQuery
    {
        private readonly string name;

        public AllCustomersWithGivenNameQuery(string name)
        {
            this.name = name;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }
    }
}