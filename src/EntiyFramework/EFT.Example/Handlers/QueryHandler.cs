﻿namespace EFT.Example.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EFT.Example.Context;

    public class QueryHandler
    {
        // TODO: when mapping code is ready uncomment this code
        //public IEnumerable<CustomerDto> Execute(AllCustomersWithGivenNameQuery query)
        //{
        //    using (var billingsContext = new BillingsDbContext())
        //    {
        //        return
        //            billingsContext.Customers.AsQueryable()
        //                .Include(c => c.Groups)
        //                .Where(c => c.Name.Equals(query.Name, StringComparison.InvariantCultureIgnoreCase))
        //                .Projec()
        //                .To<CustomerDto>()
        //                .ToList();
        //    }
        //}

        public IEnumerable<CustomerDto> Execute(CustomerWithCreditCardsExpiresCurrentYearQuery expiresCurrentYearQuery)
        {
            using (var billingsContext = new BillingsDbContext())
            {
                return Enumerable.Empty<CustomerDto>();
            }
        }
    }
}