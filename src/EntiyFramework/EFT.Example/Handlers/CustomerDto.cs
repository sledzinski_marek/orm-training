﻿namespace EFT.Example.Handlers
{
    public class CustomerDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Country from customers Address
        /// </summary>
        /// <returns></returns>
        public string Country { get; set; }
    }
}