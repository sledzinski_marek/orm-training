﻿namespace EFT.Example.Handlers
{
    using EFT.Example.Context;
    using EFT.Models;

    public class CommandHandler
    {
        public void Execute(ModifyCustomerInvoicingAddressCommand command)
        {
            using (var billingsContext  = new BillingsDbContext())
            {
                // code here
            }
        }
    }
}