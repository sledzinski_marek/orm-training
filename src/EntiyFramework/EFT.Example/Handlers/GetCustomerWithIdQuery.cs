﻿namespace EFT.Example.Handlers
{
    using System;

    public class GetCustomerWithIdQuery
    {
        private readonly int customerId;

        public GetCustomerWithIdQuery(int customerId)
        {
            this.customerId = customerId;
        }

        public int CustomerId
        {
            get
            {
                return this.customerId;
            }
        }
    }
}