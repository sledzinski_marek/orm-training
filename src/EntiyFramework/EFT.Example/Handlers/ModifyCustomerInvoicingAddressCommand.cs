﻿namespace EFT.Example.Handlers
{
    using System;

    public class ModifyCustomerInvoicingAddressCommand : ICommand
    {
        private readonly int customerId;

        private readonly string country;

        private readonly string city;

        private readonly string postalCode;

        public ModifyCustomerInvoicingAddressCommand(int customerId, string country, string city, string postalCode)
        {
            this.customerId = customerId;
            this.country = country;
            this.city = city;
            this.postalCode = postalCode;
        }

        public int CustomerId
        {
            get
            {
                return this.customerId;
            }
        }

        public string Country
        {
            get
            {
                return this.country;
            }
        }

        public string City
        {
            get
            {
                return this.city;
            }
        }

        public string PostalCode
        {
            get
            {
                return this.postalCode;
            }
        }
    }
}