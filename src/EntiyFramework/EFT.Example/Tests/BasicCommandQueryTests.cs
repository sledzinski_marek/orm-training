﻿namespace EFT.Example.Tests
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    using EFT.Example.Configuration;
    using EFT.Example.Context;
    using EFT.Example.Handlers;
    using EFT.Models;

    using NUnit.Framework;

    [TestFixture]
    public class BasicCommandQueryTests
    {
        public BasicCommandQueryTests()
        {
            AutoMapperConfiguration.RegisterKnownMappings();
        }

        [TestFixtureSetUp]
        public void DropDataBaseBeforeTest()
        {
            // TODO: if mappings are ready go to ForTestSeedingInitializer class
            // and uncomment existing code
            Database.SetInitializer(new ForTestSeedingInitializer());
        }

        // TODO: make tests green !!
        //[Test]
        //public void ShouldChangeCustomerAddressData()
        //{
        //    // given
        //    var targetAddress = new Address("GB", "London", "123-312");
        //    var commandHandler = new CommandHandler();
        //    Func<CustomerDto> getCustomerFiveFunc = () => (new QueryHandler()).Execute(new AllCustomersWithGivenNameQuery("Customer5")).First();

        //    // when
        //    (new CommandHandler())
        //        .Execute(
        //            new ModifyCustomerInvoicingAddressCommand(
        //                getCustomerFiveFunc.Invoke().Id,
        //                targetAddress.Country,
        //                targetAddress.City,
        //                targetAddress.PostalCode));

        //    // then
        //    var actualCountry = getCustomerFiveFunc.Invoke().Country;
        //    Assert.That(actualCountry, Is.EqualTo(targetAddress.Country));
        //}

        //[Test]
        //public void ShouldFindExactly3CustomersWithExpiryIn2014()
        //{
        //    // given
        //    var query = new CustomerWithCreditCardsExpiresCurrentYearQuery();

        //    // when
        //    var result = (new QueryHandler()).Execute(query);

        //    // then
        //    Assert.That(result.Count(), Is.EqualTo(3));
        //}
    }
}