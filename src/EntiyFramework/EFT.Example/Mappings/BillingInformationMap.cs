﻿namespace EFT.Example.Mappings
{
    using System.Data.Entity.ModelConfiguration;

    using EFT.Models;

    public class BillingInformationMap : EntityTypeConfiguration<BillingInformation>
    {
        public BillingInformationMap()
        {
            HasKey(b => b.Id); // can be done by convention

            // billing information cannot exist witout owner
            // but Customer without billing information can exist - 
            // (they are not our favourite type of customers :) )
            // (in fact Cusomer entity does not have navigation to Billing Order)
            // and we map here both sides of relation
            HasRequired(b => b.Owner)
                .WithMany()
                .HasForeignKey(b => b.OwnerId)
                .WillCascadeOnDelete(false);
        }
    }
}