﻿namespace EFT.Example.Configuration
{
    using EFT.Example.Handlers;
    using EFT.Models;

    public class AutoMapperConfiguration
    {
        public static void RegisterKnownMappings()
        {
            AutoMapper.Mapper.CreateMap<Customer, CustomerDto>()
                .ForMember(c => c.Country, mce => mce.MapFrom(co => co.InvoicingAddress.Country));
        }
    }
}