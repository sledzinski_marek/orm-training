﻿namespace EFT.Example.Context
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Linq;

    using EFT.Models;

    public class ForTestSeedingInitializer : DropCreateDatabaseAlways<BillingsDbContext>
    {
        protected override void Seed(BillingsDbContext context)
        {
            //var groups = this.ProduceGroups();
            //context.CustomerGroups.AddRange(groups);
            //context.SaveChanges();

            //var customers =
            //    this.ProduceCustomers(groups.First(g => g.Name == "Premium"), groups.First(g => g.Name == "Normal"))
            //        .ToList();
            //context.Customers.AddRange(customers);
            //context.SaveChanges();

            //var billings = this.ProduceBillings(customers).ToList();
            //context.BillingInformations.AddRange(billings);
            //context.SaveChanges();

            //base.Seed(context);
        }

        private IEnumerable<BillingInformation> ProduceBillings(List<Customer> customers)
        {
            int counter = 0;
            foreach (var customer in customers.Take(70))
            {
                BillingInformation info = (++counter) % 3 == 0
                                              ? new CreditCardBilling(customer, "12321123", 10, 2020)
                                              : new AccountBilling(customer, "ACC3434343", "562343") as BillingInformation;
                yield return info;
            }

            foreach (var customer in customers.Skip(80).Take(3))
            {
                yield return new CreditCardBilling(customer, "24245245", 10, 2014);
            }
        }

        private IEnumerable<CustomerGroup> ProduceGroups()
        {
            return new[]
                       {
                           new CustomerGroup("Premium"),
                           new CustomerGroup("Normal"),
                       };
        }

        private IEnumerable<Customer> ProduceCustomers(CustomerGroup premiumGroup, CustomerGroup normalGroup)
        {
            for (int counter = 0; counter < 100; counter++)
            {
                var groups = new Collection<CustomerGroup> { normalGroup };

                if (counter % 3 == 0)
                {
                    groups.Add(premiumGroup);
                }

                yield return new Customer(string.Format("Customer{0}", counter), new Address("PL", "GL", "12-34"), groups);
            }
        }
    }
}