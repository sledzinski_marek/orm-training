﻿namespace EFT.Example.Context
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    using EFT.Models;

    public class BillingsDbContext : DbContext
    {
        public BillingsDbContext()
           : base("BillingsInfoDatabase")
        {
        }

        // TODO: add proper DBSets<T> to have access to entitites collections
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(this.GetType().Assembly);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}