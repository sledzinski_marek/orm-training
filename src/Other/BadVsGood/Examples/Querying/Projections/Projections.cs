﻿namespace Examples.Querying.Projections
{
    using System.Linq;

    using AutoMapper.QueryableExtensions;

    using Examples.Mapping.ExtraneusNavigations;

    public class Projections
    {
        public void DoSomething()
        {
            using (var context = new CoursesDbContext())
            {
                var data = context.Courses.ToList();
                foreach (var course in data)
                {
                    // convert to DTO
                }

            }
        }
    }

    public class BetterProjection
    {
        public void DoSomething()
        {
            using (var context = new CoursesDbContext())
            {
                var data = context.Courses.Select(c => new CourseDto { Id = c.Id, Name = c.Name });
();
            }
        }
    }

    public class EvenBetterProjection
    {
        public void DoSomething()
        {
            using (var context = new CoursesDbContext())
            {
                // with automapper
                var data = context.Courses.Project().To<CourseDto>();
            }
        }
    }

    public class CourseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}