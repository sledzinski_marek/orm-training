﻿namespace Examples.Querying.CartesianOrUnion
{
    using System.Data.Entity;
    using System.Linq;

    using Examples.Mapping.ExtraneusNavigations;

    public class CartesianOrUnion
    {
        public void GetData()
        {
            using (var context = new CoursesDbContext())
            {
                var course =
                    context.Courses.AsQueryable()
                        .Include(c => c.Enrollemnts)
                        .Include(c => c.Instructors)
                        .Single(c => c.Id == 5);
                // EF - union all, NH - cartesiona product (when no relationship between tables)
            }
        }
    }

    public class SimplerProdcut
    {
        public void GetData()
        {
            using (var context = new CoursesDbContext())
            {
                var course = context.Courses.Single(c => c.Id == 5);

                var enrollments = context.Enrollments.Where(e => e.CourseId == course.Id);

                // .... 
                // more predictable SQL
            }
        }
    }
}