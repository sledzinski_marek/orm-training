﻿namespace Examples.Querying.ExcessiveRoundtrips
{
    using System.Linq;

    using EntityFramework.Extensions;

    using Examples.Mapping.ExtraneusNavigations;

    public class ExcessiveRoundTrips
    {
        public void DoSomething()
        {
            using (var context = new CoursesDbContext())
            {
                context.Courses.Where(c => c.Id > 45).ToList();

                context.Enrollments.Where(e => e.CourseId > 4).ToList();

                context.Instrcutors.Where(i => i.Id == 4).ToList();
            }
        }
    }

    public class LessExcessiveRoundTrips
    {
        public void DoSomething()
        {
            using (var context = new CoursesDbContext())
            {
                context.Courses.Where(c => c.Id > 45).Future();

                context.Enrollments.Where(e => e.CourseId > 4).Future();

                context.Instrcutors.Where(i => i.Id == 4).Future();
                // nuget - Extensions, maybe in EF7, is in NH

            // execute when first ToList, First etc called
            }
        }
    }
}