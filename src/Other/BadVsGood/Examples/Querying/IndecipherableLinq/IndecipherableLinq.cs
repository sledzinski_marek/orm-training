﻿namespace Examples.Querying.IndecipherableLinq
{
    using System.Linq;
    using System.Runtime.Remoting.Contexts;
    using System.Security.Cryptography;
    using System.Text.RegularExpressions;

    using Examples.Mapping.ExtraneusNavigations;

    public class IndecipherableLinq
    {
        public void DoSomthing()
        {
            using (var context = new CoursesDbContext())
            {
           //     var data = from istructor in context.Instrcutors join enr in context.Enrollments on  istructor.Id 
           //                Group by
           //               annonynmous objects
           //                  groups 
           //                not sure whati it means - not sure what slq will be generated

            }
        }
    }

    public class SimpleSqlInstead
    {
        public void DoSomthing()
        {
            using (var context = new CoursesDbContext())
            {
                context.Database.SqlQuery<Course>(@"
                                                      SELECT 
                                                            Id = tabIDCol,
                                                            Name = tabNameCol
                                                      FROM
                                                           tabOne INNER JOIN tabTwo
                                                       ORDER BY .....
                                                  ");
            }
        }
    }
}