﻿namespace Examples.Querying
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Examples.Mapping.ExtraneusNavigations;

    public class NplusOneLazyLoading
    {
        public static void GetData(int instructorId)
        {
            Instructor data;
            using (var context = new CoursesDbContext())
            {
                data = context.Instrcutors.First(i => i.Id == 5);
            }

            // .. do somehitng...

            foreach (var course in data.Courses)
            {
                // course is lazy loaded
            }
        }
    }

    public class WithEagerLoading
    {
        public static void GetData(int instructorId)
        {
            Instructor data;
            using (var context = new CoursesDbContext())
            {
                data = context.Instrcutors.AsQueryable().Include(i => i.Courses).First(i => i.Id == 5);
            }

            // .. do somehitng...

            foreach (var course in data.Courses)
            {
                // course is loaded before
            }
        }
    }
}