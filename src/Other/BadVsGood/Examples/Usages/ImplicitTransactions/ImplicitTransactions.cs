﻿namespace Examples.Usages.ImplicitTransactions
{
    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Net.Http;
    using System.Web.Http.Results;
    using System.Web.Mvc;

    using Examples.Mapping.ExtraneusNavigations;

    using EntityState = System.Data.Entity.EntityState;
    using RedirectResult = System.Web.Mvc.RedirectResult;

    public class ImplicitTransactionsController
    {
        private readonly CoursesDbContext injectedContext;

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Course course)
        {
            try
            {
                injectedContext.Entry(course).State = EntityState.Deleted;
                injectedContext.SaveChanges();

                return new RedirectResult("deleted");
            }
            catch (DBConcurrencyException)
            {
                return new RedirectResult("concurrency");
            }
            catch (DataException)
            {
                return new RedirectResult("data");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteOtherResult(Course course)
        {
            try
            {
                using (var dbContext = new CoursesDbContext())
                {
                    dbContext.Entry(course).State = EntityState.Deleted;
                    dbContext.SaveChanges();
                }

                // we loose caching, mappings caching etc
                // and if there would be more SaveChanges 
                // it sosts time - nad we might end up with partiall success, partial failure....
                DoSomethingMore();
                return new RedirectResult("deleted");
            }
            catch (DBConcurrencyException)
            {
                return new RedirectResult("concurrency");
            }
            catch (DataException)
            {
                return new RedirectResult("data");
            }
        }

        private void DoSomethingMore()
        {
            using (var dbContext = new CoursesDbContext())
            {
                dbContext.SaveChanges();
            }
        }
    }

    // better solution

    public class TransactionalController : Controller
    {
        public CoursesDbContext InjectedContext { get; private set; } //context per request
    }

    public class ExplicitTransactionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = (TransactionalController)filterContext.Controller;

            var transaction = controller.InjectedContext.Database.BeginTransaction(IsolationLevel.Snapshot);
            filterContext.HttpContext.Items["Db.Transaction"] = transaction;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var controller = (TransactionalController)filterContext.Controller;

            var transaction = (DbContextTransaction)filterContext.HttpContext.Items["Db.Transaction"];

            try
            {
                controller.InjectedContext.SaveChanges();
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}