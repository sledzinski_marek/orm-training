﻿namespace Examples.Usages.Repositories
{
    using System.Linq;

    public interface RepositoryExample<T>
    {
        T Find(int id);

        IQueryable<T> Query<T>();

        void Delete(T entity);

        void Upsert(T entity);

        void SaveChanges();
    }

    // abstraction leakage
    // not introducing many things more than API from ORM
    // maybe usefull for testing - but how to stub IQUeryable 
}