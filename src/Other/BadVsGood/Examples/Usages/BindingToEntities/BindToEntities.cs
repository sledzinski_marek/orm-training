﻿namespace Examples.Usages.BindingToEntities
{
    using System.Data.Entity;
    using System.Web.Http;
    using System.Web.Mvc;

    public class UserEntity
    {
        public string Name { get; set; }

        public bool IsAdmin { get; set; }
    }

    internal class BindToEntities : Controller
    {
        public void CreateUser([FromUri]UserEntity user)
        {
            using (var db = new UsersDatabaseContext())
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
        }
    }

    internal class UsersDatabaseContext : DbContext
    {
        public IDbSet<UserEntity> Users { get; set; }
    }
}
