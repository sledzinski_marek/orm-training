﻿namespace Examples.Mapping.ExtraneusNavigations
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } 

        public string Name { get; set; }

        public decimal Price { get; set; }

        // Do we really need all those navigation proeprties?
        // add only those that we need
        // YAGNI
        public virtual Department Department { get; set; }

        public virtual ICollection<Enrollment> Enrollemnts { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }
    }

    public class Instructor
    {
        public int Id { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
    }

    public class Enrollment
    {
        public int CourseId { get; set; }    
    }

    public class Department
    {
    }

    public class CoursesDbContext : DbContext
    {
        public DbSet<Instructor> Instrcutors { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Enrollment> Enrollments { get; set; }
    }
}