﻿namespace Examples.Mapping.DuplicatedCodeInMappings
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Reflection;

    using Examples.Mapping.ExtraneusNavigations;

    public class Mappings : EntityTypeConfiguration<Course>
    {
        public Mappings()
        {
            Property(c => c.Price).HasPrecision(10, 3);
        }
    }

    // Better to use global context conventions 

    public class ModelBuilderWithConvention : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<PrimaryKeyPrefixNamingConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties<decimal>().Configure(config => config.HasPrecision(10, 3));
            base.OnModelCreating(modelBuilder);
        }
    }

    public class PrimaryKeyPrefixNamingConvention : Convention
    {
        public PrimaryKeyPrefixNamingConvention()
        {
            Properties()
                .Where(prop => prop.Name.EndsWith("Id"))
                .Configure(config => config.IsKey().HasColumnName("PK_" + config.ClrPropertyInfo.Name));
        }
    }
}