﻿namespace Examples.Mapping.ManyToMany
{
    using System;
    using System.Data.Entity.ModelConfiguration;

    using Examples.Mapping.ExtraneusNavigations;

    public class ManyToManyMapping : EntityTypeConfiguration<Course>
    {
         // Why should we hide the join table - it is explicit entity (load one side of joint - all courses for single instrcutor)
         // We have to look at the mapping to see what the binding really is
         // We can add to this join table new properties - IsDeleted for example
         // Having this as separate entity - might make some querying easier
         public ManyToManyMapping()
        {
            HasMany(c => c.Instructors)
                .WithMany(i => i.Courses)
                .Map(t => t.MapLeftKey("CourseId").MapRightKey("InstructorId").ToTable("CourseInstructor"));
        }


        public class CourseInstrcutor
        {
            public virtual Course Course { get; set; }

            public virtual Instructor Instuctor { get; set; }

            public DateTime Since { get; set; }
        }
}