﻿namespace Examples.Mapping.PrimitiveObsession
{
    public class PrimitiveOrder
    {
        public string Email { get; set; } 

        public decimal Total { get; set; }
    }

    // Better - encapsulate data and validation and other things in new types
    public class Email
    {
  
    }

    public class Money
    {
        
    }
}