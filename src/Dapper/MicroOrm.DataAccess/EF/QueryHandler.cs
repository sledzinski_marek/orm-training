﻿namespace MicroOrm.DataAccess.EF
{
    using System.Collections.Generic;
    using System.Linq;

    public class QueryHandler
    {
        public IEnumerable<Models.EmployeeAddressDto> QueryForEmployeesIncludingAddress()
        {
            using (var context = new AdventureWorksEntities())
            {
                var data = from emp in context.Employees
                            join bea in context.BusinessEntityAddresses on emp.BusinessEntityID equals
                                bea.BusinessEntityID
                            join adr in context.Addresses on bea.AddressID equals adr.AddressID
                            select
                                new Models.EmployeeAddressDto()
                                    {
                                        BusinessEntityId = emp.BusinessEntityID,
                                        JobTitle = emp.JobTitle,
                                        City = adr.City,
                                        AddressLine1 = adr.AddressLine1,
                                        LoginId = emp.LoginID
                                    };

                return data.ToList();
            }
        }
    }
}