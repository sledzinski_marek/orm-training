﻿namespace MicroOrm.DataAccess.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using NUnit.Framework;

    [TestFixture]
    public class DataAccessTests
    {
        private const int NumberOfSelects = 5;

        [Test]
        public void ShouldOneOfThemBeFaster()
        {
            var daperProvider = new MicroOrm.MicroQueryHandler();
            var entityProvider = new EF.QueryHandler();

            var timeOfMicro = MeasureActionTimes(() => daperProvider.QueryForEmployeesIncludingAddress(), NumberOfSelects);

            var timeOfEf = MeasureActionTimes(() => entityProvider.QueryForEmployeesIncludingAddress(), NumberOfSelects);

            Console.WriteLine("Average time: EF {0} [ms], ORM {1} [ms]", timeOfEf, timeOfMicro);

            Assert.Inconclusive(); // or is it?
        }

        private static double MeasureActionTimes(Action actionToMeasure, int countOfRuns)
        {
            if (countOfRuns < 3)
            {
                throw new ArgumentException("countOfRuns");
            }

            var watch = new Stopwatch();

            var results = new List<long>();

            for (var i = 0; i < countOfRuns; i++)
            {
                watch = Stopwatch.StartNew();
                actionToMeasure();
                results.Add(watch.ElapsedMilliseconds);
            }

            // TODO: remove Skip().Take() part
            return results.OrderBy(l => l).Skip(1).Take(results.Count - 2).Average();
        }
    }
}