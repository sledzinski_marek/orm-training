﻿namespace MicroOrm.DataAccess.MicroOrm
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using Dapper;

    using global::MicroOrm.DataAccess.Models;

    public class MicroQueryHandler
    {
        private const string ConnectionString = @"data source=.\SQLEXPRESS;initial catalog=AdventureWorks2012;integrated security=True;MultipleActiveResultSets=True;";

        public IEnumerable<Models.EmployeeAddressDto> QueryForEmployeesCount()
        {
            var sql = "SELECT Count(*) FROM [AdventureWorks2012].[HumanResources].[Employee]";

            return this.ExecuteInNewConnection<EmployeeAddressDto>(sql);
        }

        public IEnumerable<EmployeeAddressDto> QueryForEmployeesIncludingAddress()
        {
            // TODO: proper query here
            // hint: joins might be usefull  :)
            var sql = @" <sql here>";

            return this.ExecuteInNewConnection<EmployeeAddressDto>(sql);
        }

        private IEnumerable<T> ExecuteInNewConnection<T>(string sql)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                return connection.Query<T>(sql).ToList();
            }
        }
    }
}