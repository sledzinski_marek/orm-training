﻿namespace MicroOrm.DataAccess.Models
{
    public class EmployeeAddressDto
    {
        public EmployeeAddressDto()
        {
        }

        public EmployeeAddressDto(int businessEntityId, string jobTitle, string city, string addressLine1, string loginId)
        {
            this.BusinessEntityId = businessEntityId;
            this.JobTitle = jobTitle;
            this.City = city;
            this.AddressLine1 = addressLine1;
            this.LoginId = loginId;
        }

        public int BusinessEntityId { get; set; }

        public string LoginId { get; set; }

        public string JobTitle { get; set; }

        public string City { get; set; }

        public string AddressLine1 { get; set; }

        public override string ToString()
        {
            return string.Format(
                "BusinessEntityId: {0}, LoginId: {1}, JobTitle: {2}, City: {3}, Address: {4}",
                this.BusinessEntityId, 
                this.LoginId, 
                this.JobTitle,
                this.City, 
                this.AddressLine1);
        }
    }
}
