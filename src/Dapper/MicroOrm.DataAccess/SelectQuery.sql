﻿
  SELECT 
	emp.BusinessEntityID,
	emp.LoginID,
	emp.JobTitle,
	ads.City,
	ads.AddressLine1,
	ads.AddressLine2

  FROM [AdventureWorks2012].[HumanResources].[Employee] emp 
  INNER JOIN [AdventureWorks2012].[Person].[BusinessEntityAddress] bea
  ON emp.BusinessEntityId = bea.BusinessEntityId
  INNER JOIN [AdventureWorks2012].[Person].[Address] ads
  ON  bea.AddressID = ads.AddressID


