﻿namespace NHibernateTraining.Tests
{
    using System.Linq;

    using NHibernateTraining.Models;

    using NUnit.Framework;

    [TestFixture]
    public class SimpleMappingTest : TestBase
    {
        [Test]
        public void ShouldHavePropertyName()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var products = session.QueryOver<Product>().List();

                    transaction.Commit();

                    Assert.IsNotNullOrEmpty(products.First().Name);
                }
            }
        }

    }
}