﻿namespace NHibernateTraining.Tests
{
    using FluentNHibernate.Cfg;

    using NHibernate;
    using NHibernate.Cfg;

    using NHibernateTraining.Models;

    using NUnit.Framework;

    public class TestBase
    {
        protected ISessionFactory sessionFactory;

        [SetUp]
        public void Setup()
        {
            var nhConfig = new Configuration().Configure();

            var conf = Fluently.Configure(nhConfig).Mappings(m => m.FluentMappings.AddFromAssemblyOf<Storage>());

            this.sessionFactory = conf.BuildSessionFactory();
        }
    }
}