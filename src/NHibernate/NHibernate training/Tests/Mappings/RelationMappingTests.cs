﻿namespace NHibernateTraining.Tests.Mappings
{
    using System.Linq;

    using NHibernate.Criterion;

    using NHibernateTraining.Models;
    using NHibernateTraining.Tests.Common;

    using NUnit.Framework;

    [TestFixture]
    public class RelationMappingTests : TestBase
    {
        [Test]
        public void SHouldStoreHaveSetOfLocations()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var storage = session.CreateCriteria<Storage>().Add(Restrictions.Eq("Name", "Central storage")).List<Storage>().First();

                    transaction.Commit();

                    Assert.IsNotEmpty(storage.Locations);
                }
            }
        }

        [Test]
        public void ShouldProductHaveCategoriesAndCategoriesHaveProducts()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var product = session.QueryOver<Product>().List().First();

                    transaction.Commit();

                    Assert.IsNotEmpty(product.Categories);
                    Assert.IsNotEmpty(product.Categories.First().Products);
                }
            }
        }
    }
}