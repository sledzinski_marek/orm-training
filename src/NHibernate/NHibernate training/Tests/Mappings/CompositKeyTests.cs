﻿namespace NHibernateTraining.Tests.Mappings
{
    using System.Linq;

    using NHibernateTraining.Models;
    using NHibernateTraining.Tests.Common;

    using NUnit.Framework;

    public class CompositKeyTests : TestBase
    {
        [Test]
        public void ShouldProductInventoryHaveLocationAndProduct()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var inventory = session.QueryOver<ProductInventory>().List().First();

                    transaction.Commit();

                    Assert.IsNotNull(inventory.Location);
                    Assert.IsNotNull(inventory.Product);
                }
            }
        }
    }
}