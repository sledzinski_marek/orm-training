﻿namespace NHibernateTraining.Tests.Mappings
{
    using System.Linq;

    using NHibernateTraining.Models;
    using NHibernateTraining.Tests.Common;

    using NUnit.Framework;

    public class ComponentClassTests : TestBase
    {
        [Test]
        public void ShouldSorageHaveAddressAndMailingAddress()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var storage = session.QueryOver<Storage>().List().First();

                    transaction.Commit();

                    Assert.IsNotNull(storage.Address);
                    Assert.IsNotNull(storage.MailingAddress);
                    Assert.AreNotSame(storage.Address, storage.MailingAddress);
                }
            }
        }
    }
}