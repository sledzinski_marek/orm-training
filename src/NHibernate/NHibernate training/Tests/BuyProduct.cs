namespace NHibernateTraining.Tests
{
    using System;
    using System.Linq;

    public class BuyProduct
    {
        private IProductRepository prodyctRepo;

        private int quantity;

        private Guid productId;

        public BuyProduct(IProductRepository prodyctRepo, Guid productId, int quantity)
        {
            this.productId = productId;
            this.quantity = quantity;
            this.prodyctRepo = prodyctRepo;
        }

        public bool Buy()
        {
            this.prodyctRepo.BeginTransaction();

            var product = this.prodyctRepo.GetProductProductById(this.productId);

            if (!product.ProductInventories.Any(i => i.Quantity >= this.quantity))
            {
                throw new NotEnuoghProductInStoreException();
            }

            var pi = product.ProductInventories.First(i => i.Quantity > 0);

            pi.Quantity -= this.quantity;

            try
            {
                this.prodyctRepo.UpdateProduct(product);
            }
            catch (NHibernate.StaleObjectStateException ex)
            {
                return false;
            }
            this.prodyctRepo.CommitTransaction();

            return true;
        }
    }
}