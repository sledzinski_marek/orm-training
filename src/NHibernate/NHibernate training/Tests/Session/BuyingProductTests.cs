﻿namespace NHibernateTraining.Tests.Session
{
    using System.Linq;

    using NHibernateTraining.Models;
    using NHibernateTraining.Tests.Common;

    using NUnit.Framework;

    [TestFixture]
    public class BuyingProductTests : TestBase
    {
        [TestCase(100)]
        [TestCase(1)]
        public void ShouldHaveInventoryDecreasedByQuantityAfterShopping(int quantity)
        {
            int quantityBeforeUpdate = this.InitializeQuantity(quantity);

            var product = this.InitializeProduct();

            var buyProduct = new BuyProduct(new ProductRepository(this.sessionFactory.OpenSession()), product.Id, quantity);

            buyProduct.Buy();

            Assert.AreEqual(quantityBeforeUpdate - quantity, this.GetProductQuantity(product));
        }

        [Test]
        [ExpectedException(typeof(NotEnuoghProductInStoreException))]
        public void ShouldThrowNotEnoughtPRoductInStoreException()
        {
            this.InitializeQuantity(0);

            var product = this.InitializeProduct();

            var buyProduct = new BuyProduct(new ProductRepository(this.sessionFactory.OpenSession()), product.Id, 1);

            buyProduct.Buy();
        }

        #region Helpers
        private Product InitializeProduct()
        {
            Product product;
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    product = session.QueryOver<Product>().List().First();

                    transaction.Commit();
                }

                session.Evict(product);
            }
            return product;
        }

        private int GetProductQuantity(Product product)
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    transaction.Commit();
                }

                return product.ProductInventories.First().Quantity;
            }
        }

        private int InitializeQuantity(int quantity)
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var product = session.QueryOver<Product>().List().First();
                    product.ProductInventories.First().Quantity = quantity;
                    transaction.Commit();
                }
            }
            return quantity;
        }

        #endregion
    }
}