﻿namespace NHibernateTraining.Tests.Common
{
    using System;

    using NHibernate;

    using NHibernateTraining.Models;

    public class ProductRepository : IProductRepository
    {
        private readonly ISession session;

        public ProductRepository(ISession session)
        {
            this.session = session;
        }

        public void BeginTransaction()
        {
            this.session.BeginTransaction();
        }

        public void CommitTransaction()
        {
            this.session.Transaction.Commit();
        }

        public void RollbackTransaction()
        {
            this.session.Transaction.Rollback();
        }

        public Product GetProductProductById(Guid id)
        {
            return this.session.Load<Product>(id);
        }

        public void UpdateProduct(Product product)
        {
            this.session.SaveOrUpdate(product);
        }

        public void InsertProduct(Product product)
        {
            this.session.Update(product);
        }
    }
}