﻿namespace NHibernateTraining.Tests.Common
{
    using System.Reflection;

    using FluentNHibernate.Cfg;

    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Mapping.ByCode;

    using NHibernateTraining.Models;

    using NUnit.Framework;

    public class TestBase
    {
        protected ISessionFactory sessionFactory;

        [SetUp]
        public void Setup()
        {
            var nhConfig = new Configuration().Configure();

            var mapper = new ModelMapper();

            mapper.AddMappings(Assembly.GetAssembly(typeof(Storage)).GetExportedTypes());

            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

          //  var conf = Fluently.Configure(nhConfig).Mappings(m => m.FluentMappings.AddFromAssemblyOf<Storage>());

            nhConfig.AddDeserializedMapping(mapping, null);

            this.sessionFactory = nhConfig.BuildSessionFactory();
        }
    }
}