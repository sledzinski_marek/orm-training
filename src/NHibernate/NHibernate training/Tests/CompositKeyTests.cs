﻿namespace NHibernateTraining.Tests
{
    using System.Linq;

    using NHibernateTraining.Models;

    using NUnit.Framework;

    public class CompositKeyTests : TestBase
    {
        [Test]
        public void SHouldStoreHaveSetOfLocations()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var inventory = session.QueryOver<ProductInventory>().List().First();

                    transaction.Commit();

                    Assert.IsNotNull(inventory.Location);
                    Assert.IsNotNull(inventory.Product);
                }
            }
        }
    }
}