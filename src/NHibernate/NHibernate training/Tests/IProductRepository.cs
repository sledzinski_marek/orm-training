﻿namespace NHibernateTraining.Tests
{
    using System;

    using NHibernateTraining.Models;

    public interface IProductRepository
    {
        void BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();

        Product GetProductProductById(Guid id);

        void UpdateProduct(Product product);

        void InsertProduct(Product product);
    }
}