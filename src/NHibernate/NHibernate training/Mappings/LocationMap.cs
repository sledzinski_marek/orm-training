﻿namespace NHibernateTraining.Mappings
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;

    using NHibernateTraining.Models;

    public class LocationMap : ClassMapping<Location>
    {
        public LocationMap()
        {
            Id(i => i.Id, g => g.Generator(new GuidCombGeneratorDef()));
            Property(p => p.Shelf);
            Property(p => p.Bin);
        }
    }
}