﻿namespace NHibernateTraining.Mappings
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;

    using NHibernateTraining.Models;

    public class StorageMap : ClassMapping<Storage>
    {
        public StorageMap()
        {
            Id(i => i.Id, a => a.Generator(new GuidCombGeneratorDef()));
            Property(p => p.Name);
        }
    }


}