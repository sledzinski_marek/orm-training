﻿namespace NHibernateTraining.Mappings
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;

    using NHibernateTraining.Models;
    using NHibernateTraining.Models.ValueObject;

    public class ProductMap : ClassMapping<Product>
    {
        public ProductMap()
        {
            Id(i => i.Id);
            Component(p => p.Price,
                s =>
                {
                    s.Property(c => c.Value, w => w.Column("Price"));
                    s.Property(c => c.Currency, a => a.Type<NHibernate.Type.EnumStringType<Currency>>());
                }
        );

            Set(p => p.ProductInventories,
                c =>
                {
                    c.Inverse(true);
                    c.Key(col => col.Column("ProductId"));
                }, r => r.OneToMany());
        }
    }
}