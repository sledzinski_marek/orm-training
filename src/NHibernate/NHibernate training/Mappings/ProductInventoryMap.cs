﻿namespace NHibernateTraining.Mappings
{
    using NHibernate.Mapping.ByCode.Conformist;

    using NHibernateTraining.Models;

    public class ProductInventoryMap : ClassMapping<ProductInventory>
    {
        public ProductInventoryMap()
        {
            ManyToOne(inventory => inventory.Product, k => k.Column("ProductId"));
            ManyToOne(i => i.Location);
            Property(p => p.Quantity);
        }
    }
}