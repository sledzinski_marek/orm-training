﻿namespace NHibernateTraining.Mappings
{
    using System;

    [Serializable]
    public class InventoryIdentifier
    {

        public virtual Guid ProductId { get; set; }
        public virtual Guid Location { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var id = obj as InventoryIdentifier;
            if (id == null)
                return false;
            if (
            ProductId == id.ProductId && Location == id.Location)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return (
            ProductId + "|" + Location).GetHashCode();
        }
    }
}