﻿namespace NHibernateTraining.Mappings
{
    using FluentNHibernate.Mapping;

    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;

    using NHibernateTraining.Models;

    using NUnit.Framework.Constraints;

    public class CategoryMap : ClassMapping<Category>
    {
        public CategoryMap()
        {
            
            Id(i => i.Id, mapper => mapper.Generator(new GuidCombGeneratorDef()));
            Property(p=>p.Description);
            Property(p=>p.Name);
            Set(r=>r.Products,
                i =>
                    {
                        i.Cascade(Cascade.All);
                        i.Table("ProductToCategory");
                        i.Inverse(true);
                        i.Key(
                            k =>
                                   
                                    k.Column("CategoryId"));
                        
                    }, r=>r.ManyToMany(
                        mtm =>
                            
                                mtm.Columns(n=>n.Name("ProductId"))
                             
                            ));
        }
    }
}