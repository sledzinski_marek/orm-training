﻿namespace NHibernateTraining.Models
{
    using System;
    using System.Collections.Generic;

    public class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public virtual Guid Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual ISet<Product> Products { get; set; }

        public virtual void AddProduct(Product product)
        {

            if (Products.Add(product))
            {
                return;
            }

            product.AddCategory(this);
        }
    }
}