﻿namespace NHibernateTraining.Models
{
    using System;
    using System.Collections.Generic;

    public class Location
    {

      public virtual Guid Id { get; protected set; }

        public virtual int Shelf { get; set; }

        public virtual int Bin { get; set; }
    }
}