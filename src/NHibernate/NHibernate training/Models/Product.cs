﻿namespace NHibernateTraining.Models
{
    using System;
    using System.Collections.Generic;

    using NHibernateTraining.Models.ValueObject;

    public class Product
    {
        public Product()
        {

            Categories = new HashSet<Category>();
            ProductInventories = new HashSet<ProductInventory>();
        }

        public virtual Guid Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual Price Price { get; set; }

        public virtual ISet<ProductInventory> ProductInventories { get; set; }

        public virtual ISet<Category> Categories { get; set; }

        public virtual void AddCategory(Category category)
        {
            if (!Categories.Add(category))
            {
                return;
            }

            category.AddProduct(this);
        }

        public virtual void AddProductInventory(ProductInventory productInventory)
        {
            if (!ProductInventories.Add(productInventory))
            {
                return;
            }

            productInventory.Product = this;
        }
    }
}