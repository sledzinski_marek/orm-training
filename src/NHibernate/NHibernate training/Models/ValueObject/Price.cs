﻿namespace NHibernateTraining.Models.ValueObject
{
    using System.Collections.Generic;

    public class Price
    {
        private readonly Dictionary<Currency, decimal> rateMap;

        public Price()
        {
            this.rateMap = new Dictionary<Currency, decimal>
                          {
                              { Currency.EUR, 4.16m },
                              { Currency.PLN, 1.0m },
                              { Currency.USD, 3.22m }
                          };
        }

        public decimal Value { get; set; }

        public Currency Currency { get; set; }

        public decimal GetPricePLN()
        {
            return rateMap[Currency] * Value;
        }
    }
}