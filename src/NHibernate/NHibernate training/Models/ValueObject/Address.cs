﻿namespace NHibernateTraining.Models.ValueObject
{
    public class Address
    {
        public string AddressLine { get; set; }
    }
}