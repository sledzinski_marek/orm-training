﻿namespace NHibernateTraining.Models.ValueObject
{
    public enum Currency
    {
        PLN,
        EUR,
        USD
    }
}
