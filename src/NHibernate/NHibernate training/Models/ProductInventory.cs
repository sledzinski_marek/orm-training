﻿namespace NHibernateTraining.Models
{
    using System;

    using NHibernateTraining.Mappings;

    public class ProductInventory
    {
        public ProductInventory()
        {
        }

        public virtual InventoryIdentifier InventoryIdentifier { get; set; }

        public virtual int Quantity { get; set; }

        public virtual Product Product { get; set; }

        public virtual Location Location { get; set; }

        protected virtual int Version { get; set; }
    }
}