﻿namespace NHibernateTraining.Models
{
    using System;
    using System.Collections.Generic;

    using NHibernateTraining.Models.ValueObject;

    public class Storage
    {
        public Storage()
        {
            Locations = new HashSet<Location>();
        }

        public virtual Guid Id { get; set; }

        public virtual string Name { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address MailingAddress { get; set; }

        // map bidirectional fluently
        public virtual ISet<Location> Locations { get; set; }
    }
}